package com.example.roomdatabase.Fragments.Add

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.roomdatabase.R
import com.example.roomdatabase.data.User
import com.example.roomdatabase.data.UserViewModel
import kotlinx.android.synthetic.main.fragment_add_frament.*
import kotlinx.android.synthetic.main.fragment_add_frament.view.*


class AddFrament : Fragment() {

private lateinit var mUserViewModel: UserViewModel
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_add_frament, container, false)

        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        view.button.setOnClickListener{
            insertDataToDatabase()
        }
        return view
    }
private fun insertDataToDatabase(){
    val firstName = addFirstName.text.toString()
    val lastName = addLastName.text.toString()
    val age = addAge.text

    if(inputCheck(firstName, lastName, age)){
        val user = User(0, firstName, lastName, Integer.parseInt(age.toString()))
        mUserViewModel.addUser(user)
        Toast.makeText(requireContext(), "Uzivatel uspesne pridan!", Toast.LENGTH_LONG).show()
        findNavController().navigate(R.id.action_addFrament_to_listFragment)
    }
    else{
        Toast.makeText(requireContext(), "Zadejte potrebne udaje!", Toast.LENGTH_LONG).show()
    }
}
private fun inputCheck(firstName: String, lastName: String, age: Editable): Boolean{
    return !(TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName) && age.isEmpty())
}



}
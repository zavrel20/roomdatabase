package com.example.roomdatabase.Fragments.List

import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.example.roomdatabase.R
import com.example.roomdatabase.data.User
import kotlinx.android.synthetic.main.custom_row.view.*


class ListAdapter: RecyclerView.Adapter<ListAdapter.MyViewHolder>(){

    private var userList = emptyList<User>()

class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_row, parent, false))
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = userList[position]
        holder.itemView.idtext.text = currentItem.id.toString()
        holder.itemView.name.text = currentItem.firstName
        holder.itemView.surname.text = currentItem.lastname
        holder.itemView.age.text = currentItem.age.toString()

    }
    fun setData(user: List<User>){
        this.userList = user
        notifyDataSetChanged()
}
}